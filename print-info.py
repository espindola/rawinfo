#!/usr/bin/python

import sys
import subprocess

def runit(args):
    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    data = p.stdout.read();
    assert p.wait() == 0
    return data

def get_high_vals(fname):
    d = runit(["/Users/espindola/inst/dcraw/dcraw", "-x", fname])
    ret = []
    for color in d.strip().split():
        val, ratio = color.split("(")
        val = int(float(val))
        ratio = int(float(ratio.split(")")[0]))
        ret.append((val,ratio))
    return ret

def get_metadata(fname):
    d = runit(["/Users/espindola/inst/dcraw/dcraw", "-i", "-v", fname])
    return d.strip().splitlines()

def get_shutter_speed_in_seconds(metadata):
    s = [x for x in metadata if "Shutter" in x]
    assert len(s) == 1
    fields = s[0].split()
    assert len(fields) == 3
    assert fields[0] == "Shutter:"
    assert fields[2] == "sec"
    secs = fields[1]
    if "/" in secs:
      parts = secs.split("/")
      assert len(parts) == 2
      assert parts[0] == "1"
      return 1/float(parts[1])
    return float(secs)

def get_iso(metadata):
    s = [x for x in metadata if "ISO" in x]
    assert len(s) == 1
    parts = s[0].split(":")
    assert parts[0] == "ISO speed"
    return int(parts[1].strip())

fnames = sys.argv[1:]

min_val = pow(2,14)
max_val = 0

for fname in fnames:
    m = get_metadata(fname)
    i = get_iso(m)
    s = get_shutter_speed_in_seconds(m)

    h = get_high_vals(fname)
    for x in h:
        max_val = max(x[0], max_val)
        min_val = min(x[0], min_val)
    hstrs = ["%5d(%4d)" % (x[0], x[1]) for x in h]

    print "%11.8f %5d %s" % (s,i, " ".join(hstrs))

#print "%5d %5d" % (min_val, max_val)
